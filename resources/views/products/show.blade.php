@extends('layouts.app')
@section('content')
    <div class="row">
        @include('sidebar')
    <div class="col">
    <div class="card-product">
        <div class="container">
            <div class="wrapper row">
                <div class="preview col-md-6">

                    <div class="preview-pic tab-content">
                        <div class="tab-pane active text-center" id="pic-1">

                            @if(Storage::exists('images/'.$product->id . '.jpg'))
                                <img src="{{ route('product.image', ['filename' => $product->id . '.jpg']) }}" alt="" class="img-responsive card-product-info">

                            @else <img class="img-responsive" src="http://placekitten.com/400/400" alt="Card image cap">
                            @endif

{{--                            <img src="http://placekitten.com/400/252" />--}}

                        </div>
{{--                        <div class="tab-pane" id="pic-2"><img src="http://placekitten.com/400/252" /></div>--}}
{{--                        <div class="tab-pane" id="pic-3"><img src="http://placekitten.com/400/252" /></div>--}}
{{--                        <div class="tab-pane" id="pic-4"><img src="http://placekitten.com/400/252" /></div>--}}
{{--                        <div class="tab-pane" id="pic-5"><img src="http://placekitten.com/400/252" /></div>--}}
                    </div>
{{--                    <ul class="preview-thumbnail nav nav-tabs">--}}
{{--                        <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>--}}
{{--                        <li><a data-target="#pic-2" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>--}}
{{--                        <li><a data-target="#pic-3" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>--}}
{{--                        <li><a data-target="#pic-4" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>--}}
{{--                        <li><a data-target="#pic-5" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>--}}
{{--                    </ul>--}}

                </div>
                <div class="details col-md-6">
                    <h3 class="product-title"> {{ $product->name }}</h3>
                    <div class="rating">
                        <div class="stars">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                        <span class="review-no">41 <a href="#">comments</a></span>
                    </div>
{{--                    <p class="product-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>--}}
                    <h4 class="price">Price: <span>{{ $product->price }} UAH</span></h4>
                    <h4>Viscosity: <a href="#">{{ $product->viscosity }}</a></h4>
                    <h4>Category:
                        @foreach($categories as $category)
                            <a href="#"> {{$category->name}}</a>
                        @endforeach
                    </h4>
                    <div class="action">
                        <a href="{{ url('add-to-cart/'.$product->id) }}" class="add-to-cart btn btn-default" type="button">add to cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <br>
        <div class="row">
            <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Display Comments</h4>
            </div>
            <div class="card-body">
            @include('products.commentsDisplay', ['comments' => $product->comments, 'product_id' => $product->id])

            @auth
                    <hr />

                    <h4>Add comment</h4>

                    <form method="post" action="{{ route('comments.store'   ) }}">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="body"></textarea>
                    <input type="hidden" name="product_id" value="{{ $product->id }}" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Add Comment" />
                </div>
            </form>
            @endauth
            @guest
               <div class="alert alert-info"><a href="{{ route('login') }}">Авторизуйтесь</a>, чтобы комментировать</div>
            @endguest
        </div>
        </div>
        </div>
        </div>
    </div>
    </div>

@endsection