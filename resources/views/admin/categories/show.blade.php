
@extends('adminlte::page')

@section('title', 'Show Category')

@section('content_header')
    <h1>Show Category</h1>
@stop


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
                <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $category->name }}
            </div>
        </div>
    </div>
@endsection