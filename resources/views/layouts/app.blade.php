<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/shop.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>--}}
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--}}
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto"></ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
{{--                    <li><a class="nav-link" href="{{ url('cart') }}"><strong><i class="fa fa-shopping-cart"></i> CART</strong></a></li>--}}

                            <a class="btn btn-primary" href="{{ url('cart') }}"><strong><i class="fa fa-shopping-cart"></i> CART</strong></a>

                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                    @else

                        @can('user-admin')
                        <li><a class="nav-link" href="{{ route('admin.dashboard') }}"><i class="fa fa-cog"></i> ADMIN</a></li>
                        @endcan
{{--                            <div class="container">--}}

{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-12 col-sm-12 col-12 main-section">--}}
{{--                                        <div class="dropdown">--}}
{{--                                            <button type="button" class="btn btn-info" data-toggle="dropdown">--}}
{{--                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count(session('cart')) }}</span>--}}
{{--                                            </button>--}}
{{--                                            <div class="dropdown-menu">--}}
{{--                                                <div class="row total-header-section">--}}
{{--                                                    <div class="col-lg-6 col-sm-6 col-6">--}}
{{--                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count(session('cart')) }}</span>--}}
{{--                                                    </div>--}}



{{--                                                    <div class="col-lg-6 col-sm-6 col-6 total-section text-right">--}}
{{--                                                        <p>Total: <span class="text-info">$ {{ $total }}</span></p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

{{--                                                @if(session('cart'))--}}
{{--                                                    @foreach(session('cart') as $id => $details)--}}
{{--                                                        <div class="row cart-detail">--}}
{{--                                                            <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">--}}
{{--                                                                <img src="{{ $details['photo'] }}" />--}}
{{--                                                            </div>--}}
{{--                                                            <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">--}}
{{--                                                                <p>{{ $details['name'] }}</p>--}}
{{--                                                                <span class="price text-info"> ${{ $details['price'] }}</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    @endforeach--}}
{{--                                                @endif--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col-lg-12 col-sm-12 col-12 text-center checkout">--}}
{{--                                                        <a href="{{ url('cart') }}" class="btn btn-primary btn-block">View all</a>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              <i class="fa fa-user"></i>  {{ Auth::user()->name }} <span class="caret"></span>
                            </a>


                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>


    <main class="py-4">
        <div class="container">
            @yield('content')
        </div>
    </main>
</div>
@yield('scripts')
</body>
</html>