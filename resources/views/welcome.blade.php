@extends('layouts.app')
@section('content')

{{--            @if (Route::has('login'))--}}
{{--                <div class="top-right links">--}}
{{--                    @auth--}}
{{--                        <a href="{{ url('/home') }}">Home</a>--}}
{{--                    @else--}}
{{--                        <a href="{{ route('login') }}">Login</a>--}}

{{--                        @if (Route::has('register'))--}}
{{--                            <a href="{{ route('register') }}">Register</a>--}}
{{--                        @endif--}}
{{--                    @endauth--}}
{{--                </div>--}}
{{--            @endif--}}

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<div class="row">
    @include('sidebar')
    <div class="col">
        <div class="row">
    @foreach ($products as $product)
        <div class="col-12 col-md-6 col-lg-4">
            <div class="card mb-2 text-center">
                <div class="card-body">


                @if(Storage::exists('images/'.$product->id . '.jpg'))
                            <img src="{{ route('product.image', ['filename' => $product->id . '.jpg']) }}" alt="" class="card-img-top">

              @else <img class="card-img-top" src="https://dummyimage.com/120x170/55595c/fff" alt="Card image cap">
              @endif


                    <h4 class="card-title"><a href="{{ route('product.show',$product->id) }}" title="View Product">{{ $product->viscosity }}</a></h4>
                    <p class="card-text">{{ $product->name }}</p>
                    <div class="col">
                        <p class="product-card-price btn-block">{{ $product->price }} UAH</p>
                    </div>
                    <div class="row">

                        <div class="col">
                            <a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-success btn-block">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach

        </div>
        <div class="row">
        <div class="col-12">
            {!! $products->links() !!}
        </div>
        </div>
    </div>
</div>

@endsection

