<div class="col-12 col-sm-3">
    <div class="card bg-light mb-3">
        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
        <ul class="list-group category_block">
            @foreach(App\Category::all() as $cat)
                <li class="list-group-item">
                    <a href="{{url('categories',$cat)}}">{{$cat->name}}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="card bg-light mb-3 text-center">
        <div class="card-header bg-primary text-white text-uppercase">Best price</div>

        <div class="card-body">
            <img class="card-img-top" src="https://dummyimage.com/120x170/55595c/fff" />

            <h4 class="card-title"><a href="#">10w40</a></h4>
            <p class="card-text">Мастило CASTROL 5w30</p>
            <p class="product-card-price">99.00 UAH</p>
        </div>
    </div>
</div>