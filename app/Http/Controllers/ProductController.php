<?php

namespace App\Http\Controllers;


use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
function __construct()
{
$this->middleware('permission:product-list', ['only' => ['index']]);
$this->middleware('permission:product-create', ['only' => ['create','store']]);
$this->middleware('permission:product-edit', ['only' => ['edit','update']]);
$this->middleware('permission:product-delete', ['only' => ['destroy']]);
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    $products = Product::latest()->paginate(5);
    return view('admin.products.index',compact('products'))
    ->with('i', (request()->input('page', 1) - 1) * 5);
}


    public function index_general()
    {
        $products = Product::latest()->paginate(6);
        return view('welcome',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    $categories = Category::all();

    return view('admin.products.create', compact('categories'));
}


/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
request()->validate([
'name' => 'required',
'viscosity' => 'required',
'category_id' => 'required',
'in_stock' => 'required',
'price' => 'required',
]);


Product::create($request->all());


return redirect()->route('products.index')
->with('success','Product created successfully.');
}


/**
* Display the specified resource.
*
* @param  \App\Product  $product
* @return \Illuminate\Http\Response
*/
public function show(Product $product)
{
    $categories = Category::where('id', $product->category_id)->get();
    return view('admin.products.show',compact('product', 'categories'));

}

public function show_product($product)
{
    $product = Product::find($product);
    if(!$product){
        return abort(404);

    }
    $categories = Category::where('id', $product->category_id)->get();

    return view('products.show', compact('product', 'categories'));

}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Product  $product
* @return \Illuminate\Http\Response
*/
public function edit(Product $product)
{
    $categories = Category::all();
    return view('admin.products.edit',compact('product','categories'));
}


/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Product  $product
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Product $product)
{
    request()->validate([
    'name' => 'required',
    'viscosity' => 'required',
    'category_id' => 'required',
    'in_stock' => 'required',
    'price' => 'required',
]);


    $product->update($request->all());

    $file = $request->file('image');
    $filename = $product->id . '.jpg';

    if ($file) {

        $file->storeAs('images', $filename);

    }

    return redirect()->route('products.index')
        ->with('success','Product updated successfully');
}


public function destroy(Product $product)
{
    $product->delete();
    return redirect()->route('products.index')
    ->with('success','Product deleted successfully');
}

public function cart(){
    return view('products.cart');
}
public function addToCart($id){

    $product = Product::find($id);

    if(!$product) {

        abort(404);

    }

    $cart = session()->get('cart');

    // if cart is empty then this the first product
    if(!$cart) {

        $cart = [
            $id => [
                "name" => $product->name,
                "quantity" => 1,
                "price" => $product->price
            ]
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    // if cart not empty then check if this product exist then increment quantity
    if(isset($cart[$id])) {

        $cart[$id]['quantity']++;

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');

    }

    // if item not exist in cart then add to cart with quantity = 1
    $cart[$id] = [
        "name" => $product->name,
        "quantity" => 1,
        "price" => $product->price
    ];

    session()->put('cart', $cart);

    return redirect()->back()->with('success', 'Product added to cart successfully!');
}

    public function updateCart(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

//            session()->flash('success', 'Cart updated successfully');
            return redirect()->back()->with('success', 'Cart updated successfully');

        }else {
            echo 'erorr!';
        }
    }

    public function removeCart(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

//            session()->flash('success', 'Product removed successfully');
            return redirect()->back()->with('success', 'Product remove successfully!');

        }
    }

    public function getProductImage($filename)
    {
        $file = Storage::disk('local')->get("images/".$filename);

        return new Response($file, 200);

    }

}