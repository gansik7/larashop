<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'CASTROL 75w140',
            'viscosity' => '75w140',
            'category_id' => 2,
            'in_stock' => 30,
            'price' => 360.00
        ]);

        DB::table('products')->insert([
            'name' => 'CASTROL ATFII',
            'viscosity' => 'ATFII',
            'category_id' => 1,
            'in_stock' => 10,
            'price' => 130.24
        ]);
        DB::table('products')->insert([
            'name' => 'MERCEDES-BENZ ATF',
            'viscosity' => 'ATF',
            'category_id' => 1,
            'in_stock' => 10,
            'price' => 1332.45
        ]);
        DB::table('products')->insert([
            'name' => 'VAG ATF',
            'viscosity' => 'ATF',
            'category_id' => 1,
            'in_stock' => 8,
            'price' => 500.65
        ]);
        DB::table('products')->insert([
            'name' => 'CASTROL 75w90',
            'viscosity' => '75w90',
            'category_id' => 1,
            'in_stock' => 20,
            'price' => 409.44
        ]);
        DB::table('products')->insert([
            'name' => 'ELF 75w80',
            'viscosity' => '75w80',
            'category_id' => 1,
            'in_stock' => 9,
            'price' => 258.11
        ]);
        DB::table('products')->insert([
            'name' => 'EVO 75w90',
            'viscosity' => '75w90',
            'category_id' => 1,
            'in_stock' => 2,
            'price' => 172.00
        ]);
        DB::table('products')->insert([
            'name' => 'ELF 75w80',
            'viscosity' => '75w80',
            'category_id' => 1,
            'in_stock' => 6,
            'price' => 195.50
        ]);
        DB::table('products')->insert([
            'name' => 'CASTROL 5w30',
            'viscosity' => '5w30',
            'category_id' => 2,
            'in_stock' => 25,
            'price' => 210.40
        ]);
        DB::table('products')->insert([
            'name' => 'Castrol EDGE 5L',
            'viscosity' => '5w30',
            'category_id' => 2,
            'in_stock' => 6,
            'price' => 1072.50
        ]);

    }
}
