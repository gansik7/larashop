<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ProductController@index_general')->name('welcome');
Route::get('/index', 'ProductController@index_general')->name('welcome');


Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/product/{id}', 'ProductController@show_product')->name('product.show');



Route::group(['middleware' => ['auth']], function() {
    Route::resource('admin/roles','RoleController');
    Route::resource('admin/users','UserController');
    Route::resource('admin/products','ProductController');
    Route::resource('admin/categories','CategoryController');
});

Route::resource('comments','CommentController');

Route::get('cart', 'ProductController@cart');
Route::get('add-to-cart/{id}', 'ProductController@addToCart');
Route::get('update-cart/{id}', 'ProductController@updateCart');
Route::get('remove-from-cart/{id}', 'ProductController@removeCart');

//Route::get('/productimage/{filename}', [
//    'uses' => 'ProductController@getProductImage',
//    'as' => 'product.image'
//]);

Route::get('/productimage/{filename}', 'ProductController@getProductImage')->name('product.image');